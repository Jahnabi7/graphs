import { Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Dashboard';
  Case1 = [
    { name: "Jan", value: 0 },
    { name: "Feb", value: 0 },
    { name: "Mar", value: 17987 },
    { name: "April", value: 22541 },
    { name: "May", value: 21214 },
    { name: "June", value: 41008 },
    { name: "July", value: 65406 },
    { name: "Aug", value: 43983 },
    { name: "Sept", value: 35217 },
    { name: "Oct", value: 80384 },
    { name: "Nov", value: 143211 },
    { name: "Dec", value: 199163 },
  ];
  Case2 = [
    { name: "Jan", value: 0 },
    { name: "Feb", value: 0 },
    { name: "Mar", value: 180 },
    { name: "April", value: 1718 },
    { name: "May", value: 8380 },
    { name: "June", value: 18522 },
    { name: "July", value: 55078 },
    { name: "Aug", value: 78512 },
    { name: "Sept", value: 80472 },
    { name: "Oct", value: 48268 },
    { name: "Nov", value: 38772 },
    { name: "Dec", value: 21822 },
  ];
  Case3 = [
    { name: "Jan", value: 0 },
    { name: "Feb", value: 0 },
    { name: "Mar", value: 352 },
    { name: "April", value: 4613 },
    { name: "May", value: 26928 },
    { name: "June", value: 38693 },
    { name: "July", value: 69074 },
    { name: "Aug", value: 41350 },
    { name: "Sept", value: 14318 },
    { name: "Oct", value: 26106 },
    { name: "Nov", value: 34130 },
    { name: "Dec", value: 58718 },
  ];
  Case4 = [
    { name: "Jan", value: 0 },
    { name: "Feb", value: 0 },
    { name: "Mar", value: 500 },
    { name: "April", value: 5841 },
    { name: "May", value: 8952 },
    { name: "June", value: 6719 },
    { name: "July", value: 5509 },
    { name: "Aug", value: 4980 },
    { name: "Sept", value: 8232 },
    { name: "Oct", value: 18283 },
    { name: "Nov", value: 26338 },
    { name: "Dec", value: 26513 },
  ];
  Case5 = [
    { name: "Jan", value: 0 },
    { name: "Feb", value: 0 },
    { name: "Mar", value: 2858 },
    { name: "April", value: 4706 },
    { name: "May", value: 1760 },
    { name: "June", value: 649 },
    { name: "July", value: 763 },
    { name: "Aug", value: 1108 },
    { name: "Sept", value: 4044 },
    { name: "Oct", value: 23065 },
    { name: "Nov", value: 12155 },
    { name: "Dec", value: 53135 },
  ];
  Case6 = [
    { name: "Jan", value: 0 },
    { name: "Feb", value: 0 },
    { name: "Mar", value: 4341 },
    { name: "April", value: 1025 },
    { name: "May", value: 535 },
    { name: "June", value: 0 },
    { name: "July", value: 1261 },
    { name: "Aug", value: 5313 },
    { name: "Sept", value: 3841 },
    { name: "Oct", value: 47637 },
    { name: "Nov", value: 9670 },
    { name: "Dec", value: 11295 },
  ];
  Case7 = [
    { name: "Jan", value: 0 },
    { name: "Feb", value: 0 },
    { name: "Mar", value: 4050 },
    { name: "April", value: 2091 },
    { name: "May", value: 516 },
    { name: "June", value: 174 },
    { name: "July", value: 288 },
    { name: "Aug", value: 1444 },
    { name: "Sept", value: 1494 },
    { name: "Oct", value: 26829 },
    { name: "Nov", value: 20646 },
    { name: "Dec", value: 11210 },
  ];
  Case8 = [
    { name: "Jan", value: 0 },
    { name: "Feb", value: 0 },
    { name: "Mar", value: 5043 },
    { name: "April", value: 1633 },
    { name: "May", value: 641 },
    { name: "June", value: 237 },
    { name: "July", value: 2898 },
    { name: "Aug", value: 11915 },
    { name: "Sept", value: 5044 },
    { name: "Oct", value: 24273 },
    { name: "Nov", value: 5349 },
    { name: "Dec", value: 7717 },
  ];
  Case9 = [
    { name: "Jan", value: 0 },
    { name: "Feb", value: 0 },
    { name: "Mar", value: 4615 },
    { name: "April", value: 1304 },
    { name: "May", value: 738 },
    { name: "June", value: 262 },
    { name: "July", value: 902 },
    { name: "Aug", value: 785 },
    { name: "Sept", value: 2089 },
    { name: "Oct", value: 18681 },
    { name: "Nov", value: 11169 },
    { name: "Dec", value: 22459 },
  ];
  Case10 = [
    { name: "Jan", value: 0 },
    { name: "Feb", value: 0 },
    { name: "Mar", value: 94 },
    { name: "April", value: 218 },
    { name: "May", value: 1322 },
    { name: "June", value: 4149 },
    { name: "July", value: 8670 },
    { name: "Aug", value: 9394 },
    { name: "Sept", value: 7018 },
    { name: "Oct", value: 11187 },
    { name: "Nov", value: 10023 },
    { name: "Dec", value: 11015 },
  ];
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
}
